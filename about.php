<html>
<head>
<style>@import 'style.css'</style>
</head>

<body>
<?php

require_once 'lib/class.About.php';
require_once 'lib/conn.php';

$about = new About($conn);

$title = $about->getTitle();
$text = $about->getText();


?>
<div class='block'>

<div id='about' class ='content'>

<h1 align='center'><?php echo $title; ?></h1>

<p align='center'><?php echo $text; ?></p>

</div>

</div>
</body>


</html>
