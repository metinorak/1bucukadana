<html>
   <title></title>
   <style>
   @media (max-width: 1280px){
     #cu-footer{
       display: none;
     }
   }

   </style>

</head>
<body>

<?php

require_once 'lib/class.Footer.php';
require_once 'lib/class.Social.php';
require_once 'lib/conn.php';

// Footer Informations
$footer = new Footer($conn);

$address = $footer->get("address");
$email = $footer->get("email");
$phone = $footer->get("phone");
$footer_message = $footer->get("footer_message");


// Social Media Links
$social = new Social($conn);

$facebook_url = $social->get("facebook");
$twitter_url = $social->get("twitter");
$instagram_url = $social->get("instagram");
$youtube_url = $social->get("youtube");


?>

<footer>

<div class='content'>
 <div class="w3-row">
  <div class="w3-col l2 m3">
    <img id="cu-footer" align='left' style="margin-top:30px" width="70%" src='img/electromobile.png'>
	<img id="cu-footer" align='left' style="margin-top:30px" width="70%" src='img/cu.png'>
  </div>

  <div class="w3-col l3 m3">
<h3>Sayfalar</h3>
<a href="index.php"><li>Ana Sayfa</li></a>
<a href="news.php"><li>Basında Biz</li></a>
<a href="index.php#about"><li>Hakkımızda</li></a>
<a href="sponsor.php"><li>Sponsorlarımız</li></a>
<a href="team.php"><li>Takım</li></a>
<a href="gallery.php"><li>Galeri</li></a>


  </div>
  <div class="w3-col l4 m6">
<h3>İletişim</h3>
               <li><img width="20px" height="20px" src="img/location.png"> <?php echo $address; ?></li>
               <li><img width="20px" height="20px" src="img/telephone.png">  Tel: <?php echo $phone; ?></li>
               <li><img width="20px" height="20px" src="img/mail.png">   Mail: <?php echo $email; ?></li>

  </div>

<div class="w3-col l3 m3 w3-center w3-margin-top">
  <a target="_blank" href="<?php echo $facebook_url; ?>">
  <img style="margin-top:70px" class="w3-hover-opacity" width="50px" height="50px" src="img/facebookblack.png" alt="">
  </a>
  <a target="_blank" href="<?php echo $twitter_url;?>">
	<img style="margin-top:70px" class="w3-hover-opacity" width="50px" height="50px" src="img/twitterblack.png" alt="">
  </a>
  <a target="_blank" href="<?php echo $youtube_url; ?>">
	<img style="margin-top:70px" class="w3-hover-opacity" width="50px" height="50px" src="img/youtubeblack.png" alt="">
  </a>
  <a target="_blank" href="<?php echo $instagram_url; ?>">
	<img style="margin-top:70px" class="w3-hover-opacity" width="50px" height="50px" src="img/instagramblack.png" alt="">
  </a>
</div>
	
<div class="w3-row w3-center"><?php echo date('Y')." © ".$footer_message; ?></div>
	 
</div> 
	</div>
</footer>
</body>
</html>
