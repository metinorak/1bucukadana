<html>
<head>
<style>@import 'style.css'</style>
</head>


<body>

<?php

require_once 'lib/class.Project.php';
require_once 'lib/conn.php';

$project = new Project($conn);

$title = $project->getTitle();
$text = $project->getText();


?>

<div class='dark-block'>

<div id='project' class ='content'>

<h1 align='center'><?php echo $title; ?></h1>

<p align='center'><?php echo $text; ?></p>

</div>

</div>
</body>


</html>
