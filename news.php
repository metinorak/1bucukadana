<html>
<head>
<title>1,5 Adana - Basında Biz</title>
<style>@import 'style.css'</style>

</head>

<body>
<div class='mainframe'>
<?php require_once 'header.php'; ?>
<?php require_once 'topnav.php'; ?>

<?php

require_once 'lib/conn.php';
require_once 'lib/class.News.php';

$news = new News($conn);

$news_array = $news->getAll();

?>
	
<div class='block'>

<div id='news' class ='content'>
	
<?php

foreach($news_array as $item){
	$title = $item['title'];
	$text = $item['text'];
	$photo = $item['photo'];
	$photo = substr($photo,3);

	echo <<< _END

		<div class="w3-row">
	<div class="w3-rest">
		<h1>$title</h1>
		</div>
	</div>
	
	<div class="w3-row">
		<div class="w3-col m4 l4"><img width="100%" style="margin-bottom:10px" src="$photo"></div>
		<div class="w3-col m8 l8">
		<div class="w3-container">$text</div>
		</div>
	</div>
	<hr style="border:1px solid black">
_END;
}

?>
	</div>

</div>
	
	
<?php require_once 'footer.php'; ?>


</div>

</body>



</html>
