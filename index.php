<?php
/*
* Date: November 2017
* Creator: Metin Orak && Bakican Boydaş
*
*/
?>

<html>
<head>
<title>1,5 Adana - Çukurova Üniversitesi Elektromobile</title>
<style>@import 'style.css'</style>

</head>

<body>
<div class='mainframe'>
<?php require_once 'header.php'; ?>
<?php require_once 'topnav.php'; ?>
<?php require_once 'slider.php'; ?>
<?php require_once 'about.php'; ?>
<?php require_once 'project.php'; ?>
<?php require_once 'form.php'; ?>	
<?php require_once 'footer.php'; ?>

</div>

</body>
</html>
