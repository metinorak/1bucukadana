<html>
<head>
<title>Galeri - 1,5 Adana</title> 
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<style>@import 'style.css'</style>
<style>@import 'gallery.css'</style>
</head>
<body>
<div class='mainframe'> 
<?php require_once 'header.php'; ?>
<?php require_once 'topnav.php'; ?>

<?php

require_once 'lib/class.Gallery.php';
require_once 'lib/conn.php';

$gallery = new Gallery($conn);


$gallery_array = $gallery->getAll();

?>




<h2 style="text-align:center">Galeri</h2>


<div class='w3-container'>

<?php

if(isset($_GET['page'])){
  $page = $_GET['page'];
}
else{
  $page = 1;
}


$length_of_array = count($gallery_array);
$row = ceil($length_of_array/4);

for($i=0; $i< $row; $i++){
  echo "<div class ='w3-row'>";
  for($j=0; $j<4; $j++){
    $number = 4*$i + $j +1;
    $photo = $gallery_array[4*$i + $j]['photo'];
    $photo = substr($photo,3);
    $text = $gallery_array[4 * $i + $j]['text'];
    if($photo){
      echo
      "<div class = 'w3-col l3' >".
      "<img  src='$photo' height='150px' width='90%' onclick='openModal();currentSlide($number)' class='hover-shadow cursor'>".
      "</div>";
    }

  }
  echo "</div><br>";
}
?>
</div>


<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">


  <?php
  for($i=0; $i<$length_of_array; $i++){
    $photo = $gallery_array[$i]['photo'];
    $photo = substr($photo,3);
    $text = $gallery_array[$i]['text'];

    $number = $i+1;
    echo
    "<div class ='mySlides'>".
    "<div class='numbertext'>$number / $length_of_array</div>".
    "<center>".
    "<img class='photo' src = '$photo' height='80%' onclick='currentSlide($number)'>".
    "</center>".
    "</div>";
  }

  ?>


    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>


    <?php
  for($i=0; $i<$length_of_array; $i++){
    $photo = $gallery_array[$i]['photo'];
    $photo = substr($photo,3);
    $text = $gallery_array[$i]['text'];

    $number = $i+1;
    if($photo){
      echo
      "<div class ='w3-col l3 m4'>".
      "<img class='demo cursor' src = '$photo' height = '140px' width='90%' onclick='currentSlide($number)'>".
      "</div>";
    }
  }

  ?>

  </div>
</div>

<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
  $('html, body').animate({ scrollTop: 0 }, 'fast');
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
  <?php require_once 'footer.php'; ?>
	</div>
</body>
</html>