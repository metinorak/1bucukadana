<html>
<head>
<title>1,5 Adana - Çukurova Üniversitesi Elektromobil</title>
<style>@import 'style.css'</style>

</head>

<body>
<div class='mainframe'>
<?php require_once 'header.php'; ?>
<?php require_once 'topnav.php'; ?>


	
	<?php

require_once 'lib/conn.php';
require_once 'lib/class.Team.php';

$team = new Team($conn);

$team_array = $team->getAll();

?>
	
	
	
	
	
<div class='block'>

<div id='team' class ='content'>


	<?php


$length_of_array = count($team_array);
$row = ceil($length_of_array/4);

for($i = 0; $i< $row; $i++){
  echo "<div class='w3-row-padding'>";
  for($j=0; $j<4; $j++){
    $name = $team_array[$i*4+$j]['name'];
    $department = $team_array[$i*4+$j]['department'];
    $role = $team_array[$i*4+$j]['role'];
    $photo = $team_array[$i*4+$j]['photo'];
    $photo = substr($photo,3);
    if($name){
    	echo <<< _END
<div style="height: auto;" class="w3-col l3 m3 card">
  <center><img src="$photo" alt="Avatar" style="width:100%; margin-top:10px; max-height: 200px;">
    <h4><b>$name</b></h4> 
    <p>$department</p> 
	<p>$role</p></center></br></br>
</div>
_END;
    }
  }
  echo "</div>";
}


?>

</div>

</div>

	<?php require_once 'footer.php'; ?>


</div>

</body>



</html>