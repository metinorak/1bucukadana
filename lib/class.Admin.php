<?php

class Admin{
    public $conn;
    function __construct($link){
        $this->conn = $link;
    }
    function __destruct(){
        $this->conn->close();
    }
    function updatePass($pass){
        $query = "UPDATE admin SET value='$pass' WHERE type='password';";
        $this->conn->query($query);
    }
    function get($type){
        $query = "SELECT value FROM admin WHERE type='$type';";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }

        $result->data_seek(0);
        $row = $result->fetch_array(MYSQLI_ASSOC);

        return $row['value'];
    } 
}
?>