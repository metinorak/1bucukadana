<?php

function encrypte($text){
    return hash("ripemd128",$text);
}


function get_post($conn,$string){
    if(get_magic_quotes_gpc()){
         $string= stripslashes($string);
    }
    $string= $conn->real_escape_string($string);
    //$string= strip_tags($string);
    return $string;
}

?>