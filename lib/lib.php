<?php

require_once 'class.About.php';
require_once 'class.Contact.php';
require_once 'class.Footer.php';
require_once 'class.News.php';
require_once 'class.Project.php';
require_once 'class.Slider.php';
require_once 'class.Social.php';
require_once 'class.Sponsors.php';
require_once 'class.Team.php';
require_once 'class.Admin.php';
?>