<?php

class About{
    public $conn;
    function __construct($link){
        $this->conn = $link;
    }
    function __destruct(){
        $this->conn->close();
    }
    function update($title,$text){
        $query = "UPDATE informations SET title='$title', definition='$text' WHERE type='about';";
        $this->conn->query($query);
    }

    function getTitle(){
        $query = "SELECT title FROM informations WHERE type='about';";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }

        $result->data_seek(0);
        $row = $result->fetch_array(MYSQLI_NUM);

        return $row[0];
    }


    function getText(){
        $query = "SELECT definition FROM informations WHERE type='about';";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }

        $result->data_seek(0);
        $row = $result->fetch_array(MYSQLI_NUM);

        return $row[0];
    }
}
?>