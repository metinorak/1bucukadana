<?php

class Footer{
    public $conn;
    function __construct($link){
        $this->conn = $link;
    } 
    function __destruct(){
        $this->conn->close();
    }   

    function update($type,$value){
        $query = "UPDATE footer SET value='$value' WHERE type='$type';";
        $this->conn->query($query);
    }
    function get($type){
        $query = "SELECT value FROM footer WHERE type='$type';";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }

        $result->data_seek(0);
        $row = $result->fetch_array(MYSQLI_NUM);

        return $row[0];
    }

}
