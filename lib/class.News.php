<?php

class News{
    public $conn;
    function __construct($link){
        $this->conn = $link;
    }
    function __destruct(){
        $this->conn->close();
    }

    function add($title,$text,$photo){
        $query = "INSERT INTO news(title,text,photo) VALUES('$title','$text','$photo');";
        $this->conn->query($query);
    }

    function delete($id){
        $query = "DELETE FROM news WHERE id=$id";
        $this->conn->query($query);
    }

    function update($id,$title,$text,$photo){
        $query = "UPDATE news SET title='$title', text ='$text', photo='$photo' WHERE id=$id";
        $this->conn->query($query);
    }
    
    function getAll(){
        $query = "SELECT id,title,text,photo FROM news ORDER BY id  DESC;";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }

        $num = $result->num_rows;

        $temp_array = array();
        for($i=0; $i<$num; $i++){
            $result->data_seek(i);
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $temp_array[] = $row;
        }

        return $temp_array;
    }
    function get($id){
        $query = "SELECT id,title,text,photo FROM news WHERE id=$id";
        $result = $this->conn->query($query);
        if(!$result){
            die($this->conn->error);
        }

        $row = $result->fetch_array(MYSQLI_ASSOC);

        return $row;
    }

}

?>