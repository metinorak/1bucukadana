<?php

class Gallery{

    public $conn;
    function __construct($link){
        $this->conn = $link;
    }
    function __destruct(){
        $this->conn->close();
    }

    function add($photo,$text){
        $query = "INSERT INTO gallery(photo,definition) VALUES('$photo','$text');";
        $this->conn->query($query);
    }


    function delete($id){
        $query = "DELETE FROM gallery WHERE id=$id;";
        $this->conn->query($query);
    }

    function update($id,$photo,$text){
        $query = "UPDATE gallery SET photo='$photo', definition ='$text' WHERE id=$id;";
        $this->conn->query($query);
    }

    function get($id){
        $query = "SELECT * FROM gallery WHERE id=$id;";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }
        $result->data_seek(0);
        $row = $result->fetch_array(MYSQLI_ASSOC);

        return $row;
    }

    function getAll(){
        $query = "SELECT * FROM gallery ORDER BY id DESC;";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }

        $num = $result->num_rows;
        $temp_array = array();

        for($i=0; $i<$num; $i++){
            $result->data_seek($i);
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $temp_array[] = $row;
        }
        return $temp_array;
    }


}



?>