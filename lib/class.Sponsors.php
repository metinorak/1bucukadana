<?php

class Sponsors{
    public $conn;
    function __construct($link){
        $this->conn = $link;
    }
    function __destruct(){
        $this->conn->close();
    }
    function add($name,$text,$photo){
        $query = "INSERT INTO sponsors(name,text,photo) VALUES('$name','$text','$photo');";
        $this->conn->query($query);
    }

    function delete($id){
        $query = "DELETE FROM sponsors WHERE id=$id;";
        $this->conn->query($query);
    }

    function update($id,$name,$text,$photo){
        $query = "UPDATE sponsors SET name ='$name', text= '$text', photo='$photo' WHERE id=$id;";
        $this->conn->query($query);
    }

    function get($id){
        $query = "SELECT * FROM sponsors WHERE id=$id;";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }
        $result->data_seek(0);
        $row = $result->fetch_array(MYSQLI_ASSOC);

        return $row;
    }
    function getAll(){
        $query = "SELECT * FROM sponsors ORDER BY id DESC;";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }

        $num = $result->num_rows;

        $temp_array = array();
        for($i=0; $i<$num; $i++){
            $result->data_seek(i);
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $temp_array[] = $row;
        }

        return $temp_array;
    }

}

?>