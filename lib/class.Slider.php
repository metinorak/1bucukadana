<?php

class Slider{

    public $conn;
    function __construct($link){
        $this->conn = $link;
    }
    function __destruct(){
        $this->conn->close();
    }

    function add($address,$text){
        $query = "INSERT INTO slider(address,text) VALUES('$address','$text');";
        $this->conn->query($query);
    }


    function delete($id){
        $query = "DELETE FROM slider WHERE id=$id;";
        $this->conn->query($query);
    }

    function update($id,$address,$text){
        $query = "UPDATE slider SET address='$address', text ='$text' WHERE id=$id;";
        $this->conn->query($query);
    }

    function get($id){
        $query = "SELECT text,address FROM slider WHERE id=$id;";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }
        $result->data_seek(0);
        $row = $result->fetch_array(MYSQLI_ASSOC);

        return $row;
    }

    function getAll(){
        $query = "SELECT * FROM slider ORDER BY id DESC;";
        $result = $this->conn->query($query);
        if(!$result){
            die("Veritabanı ulaşımında sorun var!");
        }

        $num = $result->num_rows;
        $temp_array = array();

        for($i=0; $i<$num; $i++){
            $result->data_seek($i);
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $temp_array[] = $row;
        }
        return $temp_array;
    }


}



?>