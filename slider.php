<html>

<head>
<style>@import 'style.css'</style>

</head>

<body>

<?php

require_once 'lib/class.Slider.php';
require_once 'lib/conn.php';

$slider = new Slider($conn);
$slider_array = $slider->getAll();


?>


<div class="w3-display-container">


<?php

foreach($slider_array as $item){

$photo = $item['address'];
$photo = substr($photo,3);
$text = $item['text'];


echo <<< _END
<div class="w3-display-container mySlides w3-animate-opacity">
  <img src="$photo" style="width:100%; height: 600px;">
  <div class="w3-display-bottommiddle w3-large w3-container w3-padding-16 w3-black">
    $text
  </div>
</div>
_END;

}
?>



<button class="w3-button w3-display-left w3-black" onclick="plusDivs(-1)">&#10094;</button>
<button class="w3-button w3-display-right w3-black" onclick="plusDivs(1)">&#10095;</button>

</div>

<script>
var slideIndex = 1;
var timer;
showDivs(slideIndex);


function plusDivs(n=1) {
  clearTimeout(timer);
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";
  timer = setTimeout(plusDivs,5000);
}



</script>

</body>

</html>

