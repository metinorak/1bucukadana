<html>
<head>
<style>@import 'style.css'</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>


<body>
<?php

require_once 'lib/conn.php';
require_once 'lib/class.Contact.php';

if(isset($_POST['name'])){
  $name = get_post($conn,$_POST['name']);
  $phone = get_post($conn,$_POST['phone']);
  $email = get_post($conn,$_POST['email']);
  $message = get_post($conn,$_POST['message']);

  $contact = new Contact();
  if($contact->sendMail($name,$email,$phone,$message)){
    echo "<script>alert('Mesajınızı İletilmiştir.');</script>";
    echo "<script>window.location.href='index.php';</script>";
  }
  else{
    echo "<script>alert('Mesajınızı Gönderilemedi.');</script>";
    echo "<script>window.location.href='index.php';</script>";
  }

}


?>
<div class='block'>
<div align="center" id="contact" class="content">
<form method="post" action="form.php" class="w3-container w3-card-4 w3 w3-text-black">
<h2 class="w3-center">Bize Ulaşın</h2>
 
<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="name" 
      required="required" type="text" placeholder="*Ad Soyad">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-envelope-o"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="email" type="email" placeholder="*Email">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-phone"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="phone" type="text" placeholder="Telefon">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-pencil"></i></div>
    <div class="w3-rest">
      <textarea class="w3-input w3-border" name="message" type="text" placeholder="*Mesaj"></textarea>
    </div>
</div>

<button class="w3-button w3-block w3-section w3-black w3-ripple w3-padding">Gönder</button>

</form>
	
	
	</div>
</div>
</body>


</html>
