<html>
<head>
<style>@import '../style.css'</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>

<?php
session_start();
ob_start();
if(!isset($_SESSION['username'])){
  echo "<center><a href='index.php'>Geri Dön</a><br></center>";
  die("<center>Bu sayfayı görme izniniz yok!</center>");
}


require_once '../lib/functions.php';
require_once '../lib/class.Social.php';
require_once '../lib/conn.php';

$social = new Social($conn);

if(isset($_POST['instagram']) ||
   isset($_POST['twitter']) ||
   isset($_POST['instagram']) ||
   isset($_POST['twitter']) ||
   isset($_POST['linkedin']))
   {
  $facebook_url_to_update = get_post($conn,$_POST['facebook']);
  $twitter_url_to_update = get_post($conn,$_POST['twitter']);
  $instagram_url_to_update = get_post($conn,$_POST['instagram']);
  $youtube_url_to_update = get_post($conn,$_POST['youtube']);
  $linkedin_url_to_update = get_post($conn,$_POST['linkedin']);


  $social->update("facebook",$facebook_url_to_update);
  $social->update("twitter",$twitter_url_to_update);
  $social->update("instagram",$instagram_url_to_update);
  $social->update("youtube",$youtube_url_to_update);
  $social->update("linkedin",$linkedin_url_to_update);


  echo "<script>alert('İşleminiz Başarıyla Gerçekleştirildi!')</script>";
  echo "<script>window.location.href='panel.php?section=social'</script>";

}



$facebook_url = $social->get("facebook");
$twitter_url = $social->get("twitter");
$instagram_url = $social->get("instagram");
$youtube_url = $social->get("youtube");
$linkedin_url = $social->get("linkedin");




?>



<form class="content" method="post" action="social.php">
  <label class="w3-text-black"><b>Facebook</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='facebook'
  value ='<?php echo $facebook_url; ?>'>

  <label class="w3-text-black"><b>Twitter</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='twitter'
  value ='<?php echo $twitter_url; ?>'>

  <label class="w3-text-black"><b>Instagram</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='instagram'
  value ='<?php echo $instagram_url; ?>'>

  <label class="w3-text-black"><b>Youtube</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='youtube'
  value ='<?php echo $youtube_url; ?>'>

  <label class="w3-text-black"><b>Linkedin</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='linkedin'
  value ='<?php echo $linkedin_url; ?>'>
  <br>
  <input type="submit" class="w3-button w3-block w3-khaki" value= "Güncelle">  
</form> 



</body>

</html>