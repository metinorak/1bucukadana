<html>
<head>
<style>@import '../style.css'</style>
<style>
.w3-input{
  padding: 20px;
}
</style>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>


<?php
session_start();
ob_start();
if(!isset($_SESSION['username'])){
  echo "<center><a href='index.php'>Geri Dön</a><br></center>";
  die("<center>Bu sayfayı görme izniniz yok!</center>");
}


require_once '../lib/functions.php';
require_once '../lib/class.About.php';
require_once '../lib/conn.php';
$about = new About($conn);

if(isset($_POST['title']) && isset($_POST['text'])){
  $title_to_update = get_post($conn,$_POST['title']);
  $text_to_update = get_post($conn,$_POST['text']);
  $about->update($title_to_update,$text_to_update);
  echo "<script>alert('İşleminiz Başarıyla Gerçekleştirildi!')</script>";
  echo "<script>window.location.href='panel.php?section=about'</script>";
}

$title = $about->getTitle();
$text = $about->getText();



?>

<form class="content" method="post" action="about.php">
  <label class="w3-text-black"><b>Başlık</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='title'
  value ='<?php echo $title; ?>'>

  <label class="w3-text-black"><b>İleti</b></label>
  <textarea cols="100" id="text" class="w3-input" name='text'><?php echo $text; ?></textarea>
  <br>
  <input type="submit" class="w3-button w3-block w3-khaki" value= "Güncelle">  
</form> 



</body>

</html>