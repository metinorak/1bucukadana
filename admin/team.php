<html>
<head>
<style>@import '../style.css'</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>

<?php
session_start();
ob_start();
if(!isset($_SESSION['username'])){
  echo "<center><a href='index.php'>Geri Dön</a><br></center>";
  die("<center>Bu sayfayı görme izniniz yok!</center>");
}


require_once '../lib/functions.php';
require_once '../lib/class.Team.php';
require_once '../lib/conn.php';
$team = new Team($conn);

if($_FILES && isset($_POST['name'])){

  $name = $_FILES['photo']['name'];
  $photo = "../uploads/team/".$name;
  $extension = pathinfo($photo)['extension'];
  
  while(file_exists($photo)){
    $photo = $photo."1bucukadana.".$extension;
  }

  move_uploaded_file($_FILES['photo']['tmp_name'],$photo);

  $category = get_post($conn,$_POST['category']);
  $name = get_post($conn,$_POST['name']);
  $department = get_post($conn,$_POST['department']);
  $role = get_post($conn,$_POST['role']);
  $definition = get_post($conn,$_POST['definition']);

  $team->add($category,$name,$department,$role,$definition,$photo);

  echo "<script>alert('Takım üyesi başarıyla eklendi!')</script>";
  echo "<script>window.location.href='panel.php?section=teams'</script>";
}



?>

<div class="content">

<h3>Yeni Ekle</h3>
<form class="content" method="post" action="team.php" enctype="multipart/form-data">

  <label class="w3-text-black"><b>Ad Soyad</b></label>
  <input required="required" class="w3-input w3-border w3-light-grey" type="text" name='name'>
  
  <label class="w3-text-black"><b>Bölüm</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='department'>

  <label class="w3-text-black"><b>Takım içindeki rolü</b></label>
  <input required="required" class="w3-input w3-border w3-light-grey" type="text" name='role'>

  Öğrenci:<input checked="checked"  value='Öğrenci' type="radio" name='category'>
  Akademisyen:<input  value='Akademisyen' type="radio" name='category'>
  <br>
  <label class="w3-text-black"><b>Açıklama</b></label>
  <textarea class="w3-input w3-border w3-light-grey" type="text" name='definition'></textarea>
  <br>
  <input required="required" class="w3-input w3-border w3-light-grey" type="file" name='photo'>
  <br>
  <input type="submit" class="w3-button w3-block w3-khaki" value= "Yükle">  
</form> 

<hr style="border:1px solid black">

<h3>Üyeler</h3>


<?php

$team_array = $team->getAll();


foreach($team_array as $item){
  $photo = $item['photo'];
  $name = $item['name'];
  $department = $item['department'];
  $role = $item['role'];
  $definition = $item['definition'];
  $id = $item['id'];
  $section = "Team";

  echo "<div style='border: solid 1px; width: auto;' ><img style='max-width: 300px' src='$photo'><br>".
  "İsim: $name<br>".
  "Bölüm: $department<br>".
  "Rol: $role<br>".
  "Açıklama: $definition<br>";

  echo "<button onClick='window.location.href=\"delete.php?section=Team&id=\"+$id'>Sil</button><br></div>";


}

?>

</div>

</body>

</html>
