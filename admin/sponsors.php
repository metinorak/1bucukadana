<html>
<head>
<style>@import '../style.css'</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>

<?php
session_start();
ob_start();
if(!isset($_SESSION['username'])){
  echo "<center><a href='index.php'>Geri Dön</a><br></center>";
  die("<center>Bu sayfayı görme izniniz yok!</center>");
}


require_once '../lib/functions.php';
require_once '../lib/class.Sponsors.php';
require_once '../lib/conn.php';
$sponsors = new Sponsors($conn);

if($_FILES && isset($_POST['name']) && isset($_POST['text'])){

  $name = $_FILES['photo']['name'];
  $photo = "../uploads/sponsors/".$name;

  $extension = pathinfo($photo)['extension'];
  
  while(file_exists($photo)){
    $photo = $photo."1bucukadana.".$extension;
  }

  move_uploaded_file($_FILES['photo']['tmp_name'],$photo);

  $text = get_post($conn,$_POST['text']);
  $sponsor_name = get_post($conn,$POST['text']);

  $sponsors->add($sponsor_name,$text,$photo);
  echo "<script>alert('Sponsor eklendi!')</script>";
  echo "<script>window.location.href ='panel.php?section=sponsors'</script>";

}



?>

<div class="content">

<h3>Yeni Ekle</h3>
<form class="content" method="post" action="sponsors.php" enctype="multipart/form-data">
  <label class="w3-text-black"><b>Sponsor İsmi</b></label>
  <input required='required' class="w3-input w3-border w3-light-grey" type="text" name='name'>
  <label required='required' class="w3-text-black"><b>Açıklama</b></label>
  <textarea id="text" class="w3-input w3-border w3-light-grey" name='text'></textarea>
  <br>
  <input required="required" class="w3-input w3-border w3-light-grey" type="file" name='photo'>
  <br>
  <input type="submit" class="w3-button w3-block w3-khaki" value= "Gönder">  
</form> 

<hr style="border:1px solid black">

<h3>Sponsorlar</h3>


<?php

$sponsors_array = $sponsors->getAll();

foreach($sponsors_array as $item){
  $photo = $item['photo'];
  $text = $item['text'];
  $title = $item['title'];
  $id = $item['id'];
  $section = "Sponsors";

  echo "<div style='border: solid 1px; width: auto;' >".
  "<h3>$title</h3>".
  "<img style='max-width: 300px' src='$photo'><br>".
  "$text<br>";

  echo "<button onClick='window.location.href=\"delete.php?section=Sponsors&id=\"+$id'>Sil</button><br></div>";

}

?>

</div>


</body>

</html>