<html>
<head>
<style>@import '../style.css'</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>

<?php
session_start();
ob_start();
if(!isset($_SESSION['username'])){
  echo "<center><a href='index.php'>Geri Dön</a><br></center>";
  die("<center>Bu sayfayı görme izniniz yok!</center>");
}


require_once '../lib/functions.php';
require_once '../lib/class.News.php';
require_once '../lib/conn.php';
$news = new News($conn);

if($_FILES && isset($_POST['text']) && isset($_POST['title'])){

  $name = $_FILES['photo']['name'];
  $photo = "../uploads/news/".$name;

  $extension = pathinfo($photo)['extension'];
  
  while(file_exists($photo)){
    $photo = $photo."1bucukadana.".$extension;
  }

  move_uploaded_file($_FILES['photo']['tmp_name'],$photo);

  $title = get_post($conn,$_POST['title']);
  $text = get_post($conn,$_POST['text']);

  $news->add($title,$text,$photo);
  echo "<script>alert('Haber eklendi!')</script>";
  echo "<script>window.location.href ='panel.php?section=news'</script>";

}



?>

<div class="content">

<h3>Yeni Ekle</h3>
<form class="content" method="post" action="news.php" enctype="multipart/form-data">
  <label class="w3-text-black"><b>Başlık</b></label>
  <input required='required' class="w3-input w3-border w3-light-grey" type="text" name='title'>
  <label required='required' class="w3-text-black"><b>Haber Metni</b></label>
  <textarea id="text" class="w3-input w3-border w3-light-grey" name='text'></textarea>
  <br>
  <input required="required" class="w3-input w3-border w3-light-grey" type="file" name='photo'>
  <br>
  <input type="submit" class="w3-button w3-block w3-khaki" value= "Gönder">  
</form> 

<hr style="border:1px solid black">

<h3>Haberler</h3>


<?php

$news_array = $news->getAll();

foreach($news_array as $item){
  $photo = $item['photo'];
  $text = $item['text'];
  $title = $item['title'];
  $id = $item['id'];
  $section = "News";

  echo "<div style='border: solid 1px; width: auto;'>".
  "<h4>$title</h4>".
  "<img style='max-width: 300px' src='$photo'><br>".
  "$text<br>";
  echo "<button onClick='window.location.href=\"delete.php?section=News&id=\"+$id'>Sil</button><br></div>";

}

?>

</div>
</body>

</html>