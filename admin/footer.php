<html>
<head>
<style>@import '../style.css'</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>

<?php
session_start();
ob_start();
if(!isset($_SESSION['username'])){
  echo "<center><a href='index.php'>Geri Dön</a><br></center>";
  die("<center>Bu sayfayı görme izniniz yok!</center>");
}


require_once '../lib/functions.php';
require_once '../lib/class.Footer.php';
require_once '../lib/conn.php';

$footer = new Footer($conn);

if(isset($_POST['address']) ||
   isset($_POST['phone']) ||
   isset($_POST['email']) ||
   isset($_POST['footer_message']))
   {
  $address_to_update = get_post($conn,$_POST['address']);
  $phone_to_update = get_post($conn,$_POST['phone']);
  $email_to_update = get_post($conn,$_POST['email']);
  $footer_message_to_update = get_post($conn,$_POST['footer_message']);


  $footer->update("address",$address_to_update);
  $footer->update("phone",$phone_to_update);
  $footer->update("email",$email_to_update);
  $footer->update("footer_message",$footer_message_to_update);


  echo "<script>alert('İşleminiz Başarıyla Gerçekleştirildi!')</script>";
  echo "<script>window.location.href='panel.php?section=footer'</script>";

}

$address = $footer->get("address");
$phone = $footer->get("phone");
$email = $footer->get("email");
$footer_message = $footer->get("footer_message");

?>



<form class="content" method="post" action="footer.php">
  <label class="w3-text-black"><b>Adres</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='address'
  value ='<?php echo $address; ?>'>

  <label class="w3-text-black"><b>Telefon</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='phone'
  value ='<?php echo $phone; ?>'>

  <label class="w3-text-black"><b>Email</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='email'
  value ='<?php echo $email; ?>'>

  <label class="w3-text-black"><b>Footer Mesajı</b></label>
  <input class="w3-input w3-border w3-light-grey" type="text" name='footer_message'
  value ='<?php echo $footer_message; ?>'>
  <br>
  <input type="submit" class="w3-button w3-block w3-khaki" value= "Güncelle">  
</form> 



</body>

</html>