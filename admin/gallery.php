<html>
<head>
<style>@import '../style.css'</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>

<?php
session_start();
ob_start();
if(!isset($_SESSION['username'])){
  echo "<center><a href='index.php'>Geri Dön</a><br></center>";
  die("<center>Bu sayfayı görme izniniz yok!</center>");
}


require_once '../lib/functions.php';
require_once '../lib/class.Gallery.php';
require_once '../lib/conn.php';
$gallery = new Gallery($conn);

if($_FILES && isset($_POST['text'])){

  $name = $_FILES['photo']['name'];
  $path = "../uploads/gallery/".$name;

  $extension = pathinfo($path)['extension'];
  
  while(file_exists($path)){
    $path = $path."1bucukadana.".$extension;
  }

  move_uploaded_file($_FILES['photo']['tmp_name'],$path);

  $text = get_post($conn,$_POST['text']);

  $gallery->add($path,$text);
  echo "<script>alert('Fotoğraf eklendi!')</script>";
  echo "<script>window.location.href ='panel.php?section=gallery'</script>";

}



?>
<div class="content">

<h3>Yeni Ekle</h3>
<form class="content" method="post" action="gallery.php" enctype="multipart/form-data">
  <label class="w3-text-black"><b>Resim Metni</b></label>
  <textarea class="w3-input w3-border w3-light-grey" type="text" name='text'></textarea>
  <br>
  <input required="required" class="w3-input w3-border w3-light-grey" type="file" name='photo'>
  <br>
  <input type="submit" class="w3-button w3-block w3-khaki" value= "Yükle">  
</form> 

<hr style="border:1px solid black">

<h3>Fotoğraflar</h3>


<?php

$gallery_array = $gallery->getAll();

foreach($gallery_array as $item){
  $photo = $item['photo'];
  $text = $item['definition'];
  $id = $item['id'];
  $section = "Gallery";
  echo "<div style='border: solid 1px; width: auto;' ><img style='max-width: 300px' src='$photo'><br> $text<br>";
  echo "<button onClick='window.location.href=\"delete.php?section=Gallery&id=\"+$id'>Sil</button><br></div>";

}

?>
</div>

</body>

</html>