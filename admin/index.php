<html>
<head>
<?php
    ob_start();
    session_start();
?>
<style>@import '../style.css'</style>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>

</style>

<title>1,5 Adana Yönetim Paneli</title>
</head>

<body>
<?php
if(isset($_SESSION['username'])){
    echo "<script>window.location.href='panel.php'</script>";
}
?>


<form class="w3-container" action="index.php" method="post">
<?php


require_once '../lib/conn.php';
require_once '../lib/class.Admin.php';
require_once '../lib/functions.php';

$admin = new Admin($conn);


if(isset($_POST['username']) && isset($_POST['password'])){
    $username = get_post($conn,$_POST['username']);
    $password = get_post($conn,$_POST['password']);
    $password = encrypte($password);

    if($username == $admin->get("username") && $password == $admin->get("password")){
        $_SESSION['username'] = $username;
        header("Location: panel.php");
    }
    else{        
        echo "<div class='w3-panel w3-red'>
        <h3>HATA!</h3>
        <p>Kullanıcı adı veya parola yanlış.</p>
        </div>";
    }
}

?>
<div class="w3-panel w3-black">
  <h1>Yönetim Paneli</h1>
</div>  
<label>Kullanıcı Adı</label>
<input required="required" class="w3-input" type="text" name="username">

<label>Parola</label>
<input required="required" class="w3-input" type="password" name="password">
<br>
<input type="submit" class="w3-button w3-block w3-khaki" value= "Giriş Yap">    

</form> 

</body>
</html>