<html>
<head>
<style>@import '../style.css'</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>

<?php
session_start();
ob_start();
if(!isset($_SESSION['username'])){
  echo "<center><a href='index.php'>Geri Dön</a><br></center>";
  die("<center>Bu sayfayı görme izniniz yok!</center>");
}


require_once '../lib/functions.php';
require_once '../lib/conn.php';
require_once '../lib/class.Slider.php';
require_once '../lib/class.Team.php';
require_once '../lib/class.News.php';
require_once '../lib/class.Sponsors.php';
require_once '../lib/class.Gallery.php';

if(isset($_GET['section']) && isset($_GET['id'])){
  $section = get_post($conn,$_GET['section']);
  $id = get_post($conn,$_GET['id']);


  if($section == "Slider"){
    $slider = new Slider($conn);
    $row = $slider->get($id);
    $slider->delete($id);
    $photo = $row['address'];
    unlink($photo);
  }
  else if($section == "Team"){
    $team = new Team($conn);
    $row = $team->get($id);
    $team->delete($id);
    $photo = $row['photo'];
    unlink($photo);
  }
  else if($section == "News"){
    $news = new News($conn);
    $row = $news->get($id);
    $news->delete($id);
    $photo = $row['photo'];
    unlink($photo);
  }
  else if($section == "Sponsors"){
    $sponsors = new Sponsors($conn);
    $row = $sponsors->get($id);
    $sponsors->delete($id);
    $photo = $row['photo'];
    unlink($photo);
  }
  else if($section == "Gallery"){
    $gallery = new Gallery($conn);
    $row = $gallery->get($id);
    $gallery->delete($id);
    $photo = $row['photo'];
    unlink($photo);
  }
  else{
    die("Herhangi bir işlem gerçekleşmedi.");
  }
  echo "<script>alert('Silme işlemi gerçekleştirildi')</script>";
  echo "<script>window.location.href='panel.php?section=$section'</script>";
}

?>


</body>

</html>