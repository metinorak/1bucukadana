<html>
<head>
<style>@import '../style.css'</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>

<?php
session_start();
ob_start();
if(!isset($_SESSION['username'])){
  echo "<center><a href='index.php'>Geri Dön</a><br></center>";
  die("<center>Bu sayfayı görme izniniz yok!</center>");
}


require_once '../lib/functions.php';
require_once '../lib/class.Admin.php';
require_once '../lib/conn.php';
$admin = new Admin($conn);


if(isset($_POST['current_password']) && isset($_POST['current_password2']) && isset($_POST['new_password'])){
  $current_password = get_post($conn,$_POST['current_password']);
  $current_password2 = get_post($conn,$_POST['current_password2']);
  $new_password = get_post($conn,$_POST['new_password']);

  if($current_password != $current_password2){
    echo "<script>alert('Şifreler birbirini tutmuyor. Yeniden deneyin.')</script>";
    echo "<script>window.location.href='panel.php'</script>";
  }
  else{
    $current_password = encrypte($current_password);
    $new_password = encrypte($new_password);

    if($current_password != $admin->get("password")){
      echo "<script>alert('Girdiğiniz şifre yanlış. Yeniden deneyin.')</script>";
      echo "<script>window.location.href='panel.php'</script>";
    }
    else{
      $admin->updatePass($new_password);
      echo "<script>alert('Şifreniz değiştirildi.')</script>";
      echo "<script>window.location.href='panel.php?section=password'</script>";
    }
  }
}


?>



<form class="content" method="post" action="password.php">
  <label class="w3-text-black"><b>Güncel Şifre</b></label>
  <input class="w3-input w3-border w3-light-grey" type="password" name='current_password'>

  <label class="w3-text-black"><b>Güncel Şifre (tekrar)</b></label>
  <input class="w3-input w3-border w3-light-grey" type="password" name='current_password2'>

  <label class="w3-text-black"><b>Yeni Şifre</b></label>
  <input class="w3-input w3-border w3-light-grey" type="password" name='new_password'>

  <br>
  <input type="submit" class="w3-button w3-block w3-khaki" value= "Güncelle">  
</form> 



</body>

</html>