<!DOCTYPE html>
<html>
<head>
<?php
session_start();
ob_start();
?>
<title>Admin Panel</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="../nicEdit/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
bkLib.onDomLoaded(function() {
	new nicEditor({fullPanel : true}).panelInstance('text');
});
</script>

</script>
<style>@import '../style.css'</style>

  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>


<?php
if(!isset($_SESSION['username'])){
  echo "<center><a href='index.php'>Geri Dön</a><br></center>";
  die("<center>Bu sayfayı görme izniniz yok!</center>");
}
?>


<div class="w3-sidebar w3-bar-block w3-black w3-card" style="width:200px">
  <h5 class="w3-bar-item w3-gray">Admin Panel</h5>
  <button class="w3-bar-item w3-button tablink" onclick="loadSec('About')">Hakkımızda</button>
  <button class="w3-bar-item w3-button tablink" onclick="loadSec('Project')">Proje Hakkında</button>
  <button class="w3-bar-item w3-button tablink" onclick="loadSec('Slider')">Slider</button>
  <button class="w3-bar-item w3-button tablink" onclick="loadSec('Team')">Takım</button>
  <button class="w3-bar-item w3-button tablink" onclick="loadSec('News')">Basında Biz</button>
  <button class="w3-bar-item w3-button tablink" onclick="loadSec('Gallery')">Galeri</button>
  <button class="w3-bar-item w3-button tablink" onclick="loadSec('Sponsors')">Sponsorlar</button>
  <button class="w3-bar-item w3-button tablink" onclick="loadSec('Social')">Sosyal Medya</button>
  <button class="w3-bar-item w3-button tablink" onclick="loadSec('Footer')">Footer</button>
  <button class="w3-bar-item w3-button tablink" onclick="loadSec('Password')">Şifre Değişikliği</button>
  <button class="w3-bar-item w3-button tablink" onclick="openPage('Logout')">Çıkış</button>
</div>

<div style="margin-left:200px">
  <div class="w3-padding">1,5 Adana'nın admin paneline hoşgeldiniz.<br>
  Bu paneli kullanarak site üzerinde değişiklikler yapabilirsiniz.
  </div>
  <div id="MainContent" >
  <?php
    if(isset($_GET['section'])){
    $section_page  = strtolower($_GET['section'].'.php');
    require_once $section_page;
  }
?>
  
  </div>
</div>


<script>

function openPage(page){
  if (page == "Logout"){
    window.location = "logout.php";
  }
}


function loadSec(section) {
  window.location = "panel.php?section="+section;
}

</script>

</body>
</html>

